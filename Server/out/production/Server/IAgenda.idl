module IDLFiles {

    exception ContactAlreadyExists {
        string name;
    };

    exception NonexistentContact {
        string name;
    };

    struct Contact {
        string name, phone;
    };

    typedef sequence <Contact> Contacts;

    interface Agenda {

        Contact getContact (in string name) raises (NonexistentContact);

        Contacts getContacts ();

        //Method that broadcasts the create, update and delete to the other Agendas: CUD

        void operation(in string name, in string phone, in string op) raises (ContactAlreadyExists, NonexistentContact);

        //Methods that REALLY change the list of contacts of the agendas

        void addToAgenda (in string name, in string phone) raises (ContactAlreadyExists);

        void deleteFromAgenda (in string name) raises (NonexistentContact);

        void updateAgenda (in string name, in string newPhone) raises (NonexistentContact);

        //Test if Agenda is online

        boolean testOK ();
    };
};