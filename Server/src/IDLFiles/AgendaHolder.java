package IDLFiles;

/**
* IDLFiles/AgendaHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from IAgenda.idl
* Segunda-feira, 30 de Abril de 2018 08h53min00s BRT
*/

public final class AgendaHolder implements org.omg.CORBA.portable.Streamable
{
  public IDLFiles.Agenda value = null;

  public AgendaHolder ()
  {
  }

  public AgendaHolder (IDLFiles.Agenda initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = IDLFiles.AgendaHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    IDLFiles.AgendaHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return IDLFiles.AgendaHelper.type ();
  }

}
