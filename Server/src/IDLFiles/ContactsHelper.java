package IDLFiles;


/**
* IDLFiles/ContactsHelper.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from IAgenda.idl
* Segunda-feira, 30 de Abril de 2018 08h53min00s BRT
*/

abstract public class ContactsHelper
{
  private static String  _id = "IDL:IDLFiles/Contacts:1.0";

  public static void insert (org.omg.CORBA.Any a, IDLFiles.Contact[] that)
  {
    org.omg.CORBA.portable.OutputStream out = a.create_output_stream ();
    a.type (type ());
    write (out, that);
    a.read_value (out.create_input_stream (), type ());
  }

  public static IDLFiles.Contact[] extract (org.omg.CORBA.Any a)
  {
    return read (a.create_input_stream ());
  }

  private static org.omg.CORBA.TypeCode __typeCode = null;
  synchronized public static org.omg.CORBA.TypeCode type ()
  {
    if (__typeCode == null)
    {
      __typeCode = IDLFiles.ContactHelper.type ();
      __typeCode = org.omg.CORBA.ORB.init ().create_sequence_tc (0, __typeCode);
      __typeCode = org.omg.CORBA.ORB.init ().create_alias_tc (IDLFiles.ContactsHelper.id (), "Contacts", __typeCode);
    }
    return __typeCode;
  }

  public static String id ()
  {
    return _id;
  }

  public static IDLFiles.Contact[] read (org.omg.CORBA.portable.InputStream istream)
  {
    IDLFiles.Contact value[] = null;
    int _len0 = istream.read_long ();
    value = new IDLFiles.Contact[_len0];
    for (int _o1 = 0;_o1 < value.length; ++_o1)
      value[_o1] = IDLFiles.ContactHelper.read (istream);
    return value;
  }

  public static void write (org.omg.CORBA.portable.OutputStream ostream, IDLFiles.Contact[] value)
  {
    ostream.write_long (value.length);
    for (int _i0 = 0;_i0 < value.length; ++_i0)
      IDLFiles.ContactHelper.write (ostream, value[_i0]);
  }

}
