import IDLFiles.Agenda;
import IDLFiles.AgendaHelper;
import IDLFiles.Contact;
import org.omg.CORBA.COMM_FAILURE;
import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CORBA.Object;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.omg.PortableServer.POAManagerPackage.AdapterInactive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

public class Server {

    public static void main(String args[]) {

        String a[] = new String[4];
        a[0] = "-ORBInitialPort";
        a[1] = "1050";
        a[2] = "-ORBInitialHost";
        a[3] = "localhost";
        ORB orb = ORB.init(a, null);
        boolean flag = false;
        AgendaImpl agenda = new AgendaImpl(orb);
        for (int i = 1; i < 4; i++)
            flag |= registerAgenda(orb, String.format("Agenda%d", i), agenda, flag);
        if (!flag)
            System.out.println("Failed to connect with server...");
        else {
            orb.run();
            System.out.println("Last");
        }
    }

    public static boolean registerAgenda(ORB orb, String agendaNumber, AgendaImpl agenda, boolean ignoreSet) {

        POA rootPOA;
        NamingContext naming;
        try {
            Object objPoa = orb.resolve_initial_references("RootPOA");
            rootPOA = POAHelper.narrow(objPoa);
            Object obj = orb.resolve_initial_references("NameService");
            naming = NamingContextHelper.narrow(obj);
        } catch (InvalidName invalidName) {
            System.out.println("Invalid name for Service or Root");
            return false;
        }
        Object objRef;
        try {
            objRef = rootPOA.servant_to_reference(agenda);
        } catch (ServantNotActive servantNotActive) {
            System.out.println("servantNotActive");
            return false;
        } catch (WrongPolicy wrongPolicy) {
            System.out.println("wrongPolicy");
            return false;
        }
        NameComponent[] name = { new NameComponent(agendaNumber,"") };
        Object objTest = null;
        try {
            objTest = naming.resolve(name);
        } catch (NotFound notFound) {
            System.out.println(agendaNumber + " is not registered");
        } catch (CannotProceed cannotProceed) {
            System.out.println(agendaNumber + " cannot proceed");
            return false;
        } catch (org.omg.CosNaming.NamingContextPackage.InvalidName invalidName) {
            System.out.println(agendaNumber + " invalid name");
            return false;
        }
        Agenda agendaTest = AgendaHelper.narrow(objTest);
        try {
            if (agendaTest != null && agendaTest.testOK()) {
                System.out.println(agendaNumber + " is already running");
                Contact c[] = agendaTest.getContacts();
                if (c != null)
                    agenda.setContacts(c);
                return false;
            }
        } catch (COMM_FAILURE cf) {
            System.out.println(agendaNumber + " is not running");
        }
        if (ignoreSet)
            return true;
        try {
            naming.rebind(name, objRef);
        } catch (NotFound notFound) {
            System.out.println("Rebind not found");
        } catch (CannotProceed cannotProceed) {
            System.out.println("Rebind cannot proceed");
        } catch (org.omg.CosNaming.NamingContextPackage.InvalidName invalidName) {
            System.out.println("Rebind invalid name");
        } catch (COMM_FAILURE cf) {
            System.out.println("rebind COMM_F");
        }
        try {
            rootPOA.the_POAManager().activate();
        } catch (AdapterInactive adapterInactive) {
            System.out.println("Cannot activate");
        }
        System.out.println("Server is ready ...");
        return true;
    }
}