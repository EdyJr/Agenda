import IDLFiles.*;
import org.omg.CORBA.COMM_FAILURE;
import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CORBA.Object;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import java.util.ArrayList;
import java.util.Arrays;

public class AgendaImpl extends AgendaPOA {

    private ArrayList<Contact> contacts = new ArrayList<>();
    private ORB orb;

    public AgendaImpl(ORB orb) {
        this.orb = orb;
    }

    public void setContacts(Contact[] contacts) {
        this.contacts = new ArrayList<>(Arrays.asList(contacts));
    }

    @Override
    public Contact[] getContacts() {

        System.out.println("Get Contacts");
        if (contacts == null)
            return null;
        return contacts.toArray(new Contact[contacts.size()]);
    }

    @Override
    public void operation(String name, String phone, String op)
            throws ContactAlreadyExists, NonexistentContact {

        Object obj = null;
        try {
            obj = orb.resolve_initial_references("NameService");
        } catch (InvalidName invalidName) {
            System.out.println("NameService is an invalid name");
        }
        NamingContext naming = NamingContextHelper.narrow(obj);
        for (int i = 1; i < 4; i++) {
            String an = String.format("Agenda%d", i);
            NameComponent[] nameC = { new NameComponent(an, "") };
            try {
                Object objRef = naming.resolve(nameC);
                Agenda agenda = AgendaHelper.narrow(objRef);
                switch (op) {
                    case "C":
                        agenda.addToAgenda(name, phone);
                        break;
                    case "U":
                        agenda.updateAgenda(name, phone);
                        break;
                    default:
                        agenda.deleteFromAgenda(name);
                }
            } catch (NotFound notFound) {
                System.out.println(an + " not found");
            } catch (CannotProceed cannotProceed) {
                System.out.println(an + " cannot Proceed");
            } catch (org.omg.CosNaming.NamingContextPackage.InvalidName invalidName) {
                System.out.println(an + " invalid name");
            } catch (COMM_FAILURE cf) {
                System.out.println(an + " CF IMPL");
            }
        }
    }

    int findContact(String name) {

        for (int i = 0; i < contacts.size(); i++)
            if (contacts.get(i).name.compareTo(name) == 0)
                return i;
        return -1;
    }

    public boolean addContact(String name, String phone) {

        Contact newContact = new Contact(name, phone);
        contacts.add(newContact);
        return true;
    }

    @Override
    public void addToAgenda(String name, String phone) throws ContactAlreadyExists {

        if (findContact(name) != -1)
            throw new ContactAlreadyExists(name);
        addContact(name, phone);
        System.out.println("Add contact: " + name + "; " + phone);
    }

    @Override
    public Contact getContact(String name) throws NonexistentContact {

        int pos = findContact(name);
        if (pos == -1)
            throw new NonexistentContact(name);
        return contacts.get(pos);
    }

    @Override
    public void deleteFromAgenda(String name) throws NonexistentContact {

        int pos = findContact(name);
        if (pos == -1)
            throw new NonexistentContact(name);
        contacts.remove(pos);
        System.out.println("Delete contact: " + name);
    }

    @Override
    public void updateAgenda(String name, String newPhone) throws NonexistentContact {

        int pos = findContact(name);
        if (pos == -1)
            throw new NonexistentContact(name);
        contacts.get(pos).phone = newPhone;
        System.out.println("Update contact: " + name + "; " + newPhone);
    }

    @Override
    public boolean testOK() {

        System.out.println("Test OK");
        return true;
    }
}
