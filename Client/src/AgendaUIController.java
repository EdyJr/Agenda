import IDLFiles.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import org.omg.CORBA.COMM_FAILURE;
import org.omg.CORBA.ORB;
import org.omg.CORBA.Object;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.InvalidName;
import org.omg.CosNaming.NamingContextPackage.NotFound;

public class AgendaUIController {

    @FXML TableView<Contact> contactsTable;
    @FXML TableColumn contactColumn;
    @FXML TextField phoneInsertedField;
    @FXML TextField nameInsertedField;
    @FXML TextField phoneField;
    @FXML TextField nameField;
    @FXML Button updateTableButton;
    @FXML Button createButton;
    @FXML Button SearchButton;
    @FXML Button updateButton;
    @FXML Button DeleteButton;
    @FXML Button okButton;
    @FXML Label nameEmptyLabel;
    @FXML Label agendaLabel;
    @FXML Label infoLabel;
    @FXML HBox contactBox;
    @FXML HBox phoneBox;

    private static final String defautText = "Searched contacts will be displayed here";
    private static final String existsText = "Contact already exists";
    private static final String addedtext = "Contact created and added";
    private static final String notFoundtext = "Contact not found";
    private static final String foundtext = "Contact found";
    private static final String emptyNameText = "Name can't be empty";
    private static final String deletedText = "Contact deleted";
    private static final String updatedText = "Contact updated";

    Agenda agenda;

    ORB orb;

    private ObservableList<Contact> data = FXCollections.observableArrayList();

    public void setAgenda(Agenda agenda) {

        this.agenda = agenda;
        updateTable();
    }

    public void setOrb(ORB orb) {
        this.orb = orb;
    }

    public void initUI(String agenda) {

        contactBox.setVisible(false);
        phoneBox.setVisible(false);
        nameEmptyLabel.setVisible(false);
        infoLabel.setText(defautText);
        agendaLabel.setText(agenda);
        contactsTable.setItems(data);
        contactColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<Contact, String>,
                ObservableValue<String>>) c -> c.getValue().getNameP());
        contactsTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null)
                displayContact(newValue, foundtext + ": " + newValue.name);
        });
    }

    @FXML
    public void create() {

        if (!nameInsertedField.getText().isEmpty()) {
            phoneBox.setVisible(true);
            nameEmptyLabel.setVisible(false);
        } else {
            nameEmptyLabel.setVisible(true);
            displayContact(null, defautText);
            phoneBox.setVisible(false);
        }
    }

    @FXML
    public void confirmCreation() {

        if (!testAgenda()) {
            System.out.println("Caiu");
            return;
        }
        if (!nameInsertedField.getText().isEmpty()) {
            String name = nameInsertedField.getText();
            String phone = phoneInsertedField.getText();
            try {
                agenda.operation(name, phone, "C");
            } catch (ContactAlreadyExists contactAlreadyExists) {
                try {
                    Contact c = agenda.getContact(name);
                    displayContact(c, existsText);
                } catch (NonexistentContact nonexistentContact) {
                    nonexistentContact.printStackTrace();
                }
                return;
            } catch (NonexistentContact nonexistentContact) {
                System.out.println("Impossible in create");
            } catch (COMM_FAILURE cf) {
                System.out.println("COMM Creation");
            }
            displayContact(new Contact(name, phone), addedtext + ": " + name);
        } else {
            nameEmptyLabel.setVisible(true);
            phoneBox.setVisible(false);
        }
    }

    @FXML
    public void searchContact() {

        if (!testAgenda()) {
            System.out.println("Caiu");
            return;
        }
        if (!nameInsertedField.getText().isEmpty()) {
            phoneBox.setVisible(false);
            nameEmptyLabel.setVisible(false);
            String name = nameInsertedField.getText();
            Contact c;
            try {
                c = agenda.getContact(name);
            } catch (NonexistentContact nonexistentContact) {
                displayContact(null, notFoundtext);
                phoneBox.setVisible(false);
                return;
            }
            displayContact(c, foundtext + ": " + name);
        } else {
            nameEmptyLabel.setVisible(true);
            displayContact(null, defautText);
            phoneBox.setVisible(false);
        }
    }

    @FXML
    public void deleteContact() {

        if (!testAgenda()) {
            System.out.println("Caiu");
            return;
        }
        if (!nameField.getText().isEmpty()) {
            String name = nameField.getText();
            try {
                agenda.operation(name,"", "D");
            } catch (ContactAlreadyExists contactAlreadyExists) {
                contactAlreadyExists.printStackTrace();
            } catch (NonexistentContact nonexistentContact) {
                displayContact(null, notFoundtext);
                phoneBox.setVisible(false);
                return;
            } catch (COMM_FAILURE cf) {
                System.out.println("COMM_F");
            }
            displayContact(null, deletedText);
            phoneBox.setVisible(false);
        } else
            infoLabel.setText(emptyNameText);
    }

    @FXML
    public void updateContact() {

        if (!testAgenda()) {
            System.out.println("Caiu");
            return;
        }
        if (!nameField.getText().isEmpty()) {
            String name = nameField.getText();
            String newPhone = phoneField.getText();
            try {
                agenda.operation(name, newPhone, "U");
            } catch (ContactAlreadyExists contactAlreadyExists) {
                contactAlreadyExists.printStackTrace();
            } catch (NonexistentContact nonexistentContact) {
                displayContact(null, notFoundtext);
                phoneBox.setVisible(false);
                return;
            } catch (COMM_FAILURE cf) {
                System.out.println("COMM Update");
            }
            infoLabel.setText(updatedText + ": " + name);
            phoneBox.setVisible(false);
        } else
            infoLabel.setText(emptyNameText);
    }

    @FXML
    public void updateTable() {

        if (!testAgenda()) {
            System.out.println("Caiu");
            return;
        }
        data.clear();
        System.out.println("Lista");
        for (Contact c : agenda.getContacts()) {
            System.out.println(c.name);
            data.add(new Contact(c.name, c.phone));
        }
    }

    public void displayContact(Contact c, String message) {

        infoLabel.setText(message);
        if (c == null) {
            contactBox.setVisible(false);
            return;
        }
        contactBox.setVisible(true);
        nameField.setText(c.name);
        phoneField.setText(c.phone);
    }

    public String selectAgenda() {

        Object obj = null;
        try {
            obj = orb.resolve_initial_references("NameService");
        } catch (org.omg.CORBA.ORBPackage.InvalidName invalidName) {
            invalidName.printStackTrace();
        }
        NamingContext naming = NamingContextHelper.narrow(obj);

        for (int i = 1; i < 4; i++) {
            String an = String.format("Agenda%d", i);
            NameComponent[] name = { new NameComponent(an,"") };
            Object objRef = null;
            try {
                objRef = naming.resolve(name);
            } catch (NotFound notFound) {
                System.out.println(an + " not found");
            } catch (CannotProceed cannotProceed) {
                System.out.println(an + " cannot proceed");
            } catch (InvalidName invalidName) {
                System.out.println(an + " invalid name");
            } catch (COMM_FAILURE cf) {
                System.out.println("COMM select");
            }
            Agenda agendaTest = AgendaHelper.narrow(objRef);
            try {
                if (agenda != null && agendaTest.testOK()) {
                    System.out.println(an + " is running");
                    setAgenda(agendaTest);
                    return an;
                }
            } catch (COMM_FAILURE cf) {
                System.out.println(an + " is not running");
            }
        }
        System.out.println("Failed to connect with server...");
        return null;
    }

    public boolean testAgenda() {

        String newA = "";
        try {
            agenda.testOK();
        } catch (COMM_FAILURE cf) {
            System.out.println("COM TestAgenda");
            newA = selectAgenda();
            if (newA == null)
                return false;
            agendaLabel.setText(newA);
        }
        return true;
    }
}
