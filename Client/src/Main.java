import IDLFiles.Agenda;
import IDLFiles.AgendaHelper;
import IDLFiles.Contact;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.omg.CORBA.COMM_FAILURE;
import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CORBA.Object;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        FXMLLoader loader = new FXMLLoader(getClass().getResource("AgendaUI.fxml"));
        SplitPane root = loader.load();
        AgendaUIController controller = loader.getController();

        String args[] = getParameters().getRaw().toArray(new String[0]);
        try {
            ORB orb = ORB.init(args, null);
            Object obj = orb.resolve_initial_references("NameService");
            NamingContext naming = NamingContextHelper.narrow(obj);

            boolean flag = false;
            for (int i = 1; i < 4 && !flag; i++) {
                String an = String.format("Agenda%d", i);
                NameComponent[] name = { new NameComponent(an,"") };
                Object objRef = naming.resolve(name);
                Agenda agenda = AgendaHelper.narrow(objRef);
                try {
                    if (agenda != null && agenda.testOK()) {
                        System.out.println(an + " is running");
                        controller.initUI(an);
                        controller.setAgenda(agenda);
                        controller.setOrb(orb);
                        flag = true;
                        primaryStage.setTitle("Agenda");
                        primaryStage.setScene(new Scene(root, 600, 400));
                        primaryStage.show();
                    }
                } catch (COMM_FAILURE cf) {
                    System.out.println(an + " is not running");
                }
            }
            if (!flag)
                System.out.println("Failed to connect with server...");
        } catch (InvalidName invalidName) {
            invalidName.printStackTrace();
        } catch (CannotProceed cannotProceed) {
            cannotProceed.printStackTrace();
        } catch (org.omg.CosNaming.NamingContextPackage.InvalidName invalidName) {
            invalidName.printStackTrace();
        } catch (NotFound notFound) {
            System.out.println("Not found");
        } catch (COMM_FAILURE cf) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Server Issues");
            alert.setHeaderText("Failed to connect to server");
            alert.setContentText("Maybe port and host are wrong");
            alert.showAndWait();
            return;
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
